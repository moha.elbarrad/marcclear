@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="container containerC h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="contacte_card">
                    <div class="card-header">Formulari de contacte</div>

                    <div class="card-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('enviarMail') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="nom" class="col-md-4 col-form-label">Nom</label>
                                <div class="col-md-6">
                                    <input name="nom" type="text" class="form-control inputD" required>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="correu" class="col-md-4 control-label">Correu electrònic</label>

                                <div class="col-md-6">
                                    <input name="correu" type="email" class="form-control inputD" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="assumpte" class="col-md-4 control-label">Assumpte</label>

                                <div class="col-md-6">
                                    <input name="assumpte" type="text" class="form-control inputD" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="missatge" class="col-md-4 control-label">Missatge</label>

                                <div class="col-md-6">
                                    <textarea name="missatge" rows="4" cols="32" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary change_btn">
                                        Enviar missatge
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
