@extends('layouts.master')

@section('content')
<div calss="container">
    <div class="d-flex justify-content-center h-100">
        <div id="app" class="card_dades">
            <home-admin :user="{{ Auth::user()->id }}" :ruta="'{{ env('APP_URL') }}'"></admin-home>
        </div>
    </div>
</div>
@endsection
