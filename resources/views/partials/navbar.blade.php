<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container navC">
        <a class="navbar-brand" href="/" style="color:#777"><img class="imgN" src="{{ asset('img/logo.png') }}" width="206" height="64" /></a>

        <button class="navbar-toggler" id="toggle-btn" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        @if( true || Auth::check() )
        <div class="collapse navbar-collapse llistat-index" id="navbarSupportedContent">
            <ul class="navbar-nav navbar-right">
                <li class="nav-item navP" id="gestio">
                    <form action="{{route('gestioUsuari') }}" method="GET" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">Gestó d'usuari</button>
                    </form>
                </li>
                @if(auth()->user()->tipusUsuari != 3)
                <li class="nav-item" id="contacte">
                    <form action="{{route('contacte')}}" method="GET" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">Contacte</button>

                    </form>
                </li>
                @endif
                <li class="nav-item" id="fitxarP">
                    <form action="{{route('fitxar') }}" method="GET" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">Fitxar personal</button>
                    </form>
                </li>

                <li class="nav-item" id="inici">
                    <form action="{{route('home') }}" method="GET" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">Tornar a l'inici</button>
                    </form>
                </li>

                <li class="nav-item">
                    <form action="{{ url('/logout') }}" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link nav-link" style="display:inline;cursor:pointer">Tancar la sessió</button>
                    </form>
                </li>
            </ul>
        </div>
        @endif
    </div>
</nav>

<script type="text/javascript">
    var APP_URL = window.location.href
    var res = APP_URL.split("/");
    if (res[3].startsWith("gestioUsuari")) {
        document.getElementById("gestio").style.visibility = 'hidden';
        document.getElementById("gestio").style.display = 'none';
        document.getElementById("contacte").className = "nav-item navP";
    } else if ((res[3].charAt(0) == '?') || (res[3] == '')) {
        document.getElementById("inici").style.visibility = 'hidden';
        document.getElementById("inici").style.display = 'none';
    } else if (res[3].startsWith("contacte")) {
        document.getElementById("contacte").style.visibility = 'hidden';
        document.getElementById("contacte").style.display = 'none';
    }else if (res[3].startsWith("fitxarPersonal")) {
        document.getElementById("fitxarP").style.visibility = 'hidden';
        document.getElementById("fitxarP").style.display = 'none';
    }
</script>
