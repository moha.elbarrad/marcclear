@extends('layouts.master')
@section('content')
    <div class="card loginRespo">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form method="POST" action="{{ route('loginok') }}">
				{{ csrf_field() }}
                <div>
                    <label>Usuari</label>
                    <input type="text" id="usuari" class="form-control" name="usuari" placeholder="usuari...">
                </div>
                <div>
                    <label>Contrasenya</label>
                    <input type="password" id="password" class="form-control" name="password" placeholder="contrasenya...">

                    <div class="error" id="loginUser">
                        <p>L'id o la contrasenya no coincideixen</p>
                    </div>
                </div>
                <button class="btn btn-primary" id="btn-log" type="submit" v-on:click="logRespo()">
                    <span class="glyphicon" id="icon">Login</span>
                </button>
            </form>
        </div>
    </div>
@endsection