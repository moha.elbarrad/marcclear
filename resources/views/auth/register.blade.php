@extends('layouts.login_master')

@section('content')
<div class="container h-100">
    <div class="d-flex justify-content-center h-100">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <img src="{{ asset('img/logo.png') }}" class="brand_logo" alt="Logo" width="250" height="50">
            </div>
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="usuari" class="col-md-4 col-form-label text-md-right">{{ __('Nom d\'usuari') }}</label>

                    <div class="col-md-6">
                        <input id="usuari" type="text" class="form-control{{ $errors->has('usuari') ? ' is-invalid' : '' }}" name="usuari" value="{{ old('usuari') }}" required autofocus>

                        @if ($errors->has('usuari'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('usuari') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correu electrònic') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contrasenya') }}</label>

                    <div class="col-md-6 input-group" id="show_hide_password">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirma la contrassenya') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tipusUsuari" class="col-md-4 col-form-label text-md-right">{{ __('Tipus d\'usuari') }}</label>

                    <div class="col-md-6">
                        <select class="selectR" name="tipusUsuari">
                            <option value="0">Client</option>
                            <option value="1">Proveïdor</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary login_btn">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="d-flex justify-content-center links">
                        Tornar al <a href="{{ env('APP_URL') }}/login" class="ml-2">Login</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
