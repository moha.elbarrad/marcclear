@extends('layouts.login_master')

@section('content')
<div class="container h-100">
	<div class="d-flex justify-content-center h-100">
		<div class="user_card">
			<div class="d-flex justify-content-center">
				<img src="{{ asset('img/logo.png') }}" class="brand_logo" alt="Logo" width="250" height="50">
			</div>
			<div class="d-flex justify-content-center form_container">
				<form method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}

					<div class="input-group mb-3">
						<div class="input-group-append">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input id="usuari" type="usuari" class="form-control{{ $errors->has('usuari') ? ' is-invalid' : '' }}" name="usuari" value="{{ old('usuari') }}" required autofocus>

						@if ($errors->has('usuari'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('usuari') }}</strong>
							</span>
						@endif
					</div>
					<div class="input-group mb-2">
						<div class="input-group-append">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

						@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

							<label class="form-check-label" for="remember">
								{{ __('Recorda') }}
							</label>
						</div>
					</div>
					<div class="d-flex justify-content-center mt-3 login_container">
						<button type="submit" class="btn btn-primary login_btn">
							{{ __('Login') }}
						</button>

						@if (Route::has('password.request'))
							<a class="btn btn-link" href="{{ route('password.request') }}">
								{{ __('Has oblidat la contrasenya?') }}
							</a>
						@endif
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
