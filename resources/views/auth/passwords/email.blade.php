@extends('layouts.login_master')

@section('content')
<div class="container h-100">
    <div class="d-flex justify-content-center h-100">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <img src="{{ asset('img/logo.png') }}" class="brand_logo" alt="Logo" width="250" height="50">
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                    <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correu electrònic') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary login_btn">
                            Restablir la contrasenya
                            </button>
                        </div>
                    </div>
                </form>
                <div class="mt-4">
                    <div class="d-flex justify-content-center links">
                        Tornar al <a href="{{ env('APP_URL') }}/login" class="ml-2">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
