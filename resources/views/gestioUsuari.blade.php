@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="container containerP h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="password_card">
                    <div class="card-header">Canviar contrassenya</div>

                    <div class="card-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('gestioUsuari') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }} row">
                                <label for="new-password" class="col-md-4 col-form-label inputsP">Contrasenya actual</label>
                                <div class="col-md-6">
                                    <input id="current-password" type="password" class="form-control inputCont" name="current-password" required>

                                    @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }} row">
                                <label for="new-password" class="col-md-4 control-label inputsP">Nova contrasenya</label>

                                <div class="col-md-6">
                                    <input id="new-password" type="password" class="form-control inputCont" name="new-password" required>

                                    @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new-password-confirm" class="col-md-4 control-label inputsP">Repeteix la contrasenya</label>

                                <div class="col-md-6">
                                    <input id="new-password-confirm" type="password" class="form-control inputCont" name="new-password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary change_btn">
                                        Canviar contrassenya
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

