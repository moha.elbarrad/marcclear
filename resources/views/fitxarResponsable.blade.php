@extends('layouts.master')

@section('content')
<div calss="container">
    <div class="d-flex justify-content-center h-100">
        <div id="app" class="card_dades">
            <fitxar-responsable :user="{{ Auth::user()->id }}" :ruta="'{{ env('APP_URL') }}'"></fitxar-responsable>
        </div>
    </div>
</div>
@endsection