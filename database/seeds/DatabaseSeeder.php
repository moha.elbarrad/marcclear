<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Treballador;
use App\Centre;
use App\Concepte;
use App\Client;

class DatabaseSeeder extends Seeder
{
    /**cl
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        self::seedTreballadors();
        $this->command->info('Tabla treballadors inicialitzada con datos!');

        self::seedClients();
        $this->command->info('Tabla Clients inicializada con datos!');

        self::seedCentres();
        $this->command->info('Tabla centres inicializada con datos!');

        //self::seedUsers();
        //$this->command->info('Tabla users inicializada con datos!');

        self::seedConceptes();
        $this->command->info('Tabla Conceptes inicializada con datos!');
    }

    private function seedUsers(){
        DB::table('users')->delete();

        foreach( $this->arrayUsers as $user) {
            $p = new User;
            $p->id = $user['id'];
            $p->codiTreballador = $user['usuari'];
            $p->emailTreballador = $user['email'];
            $p->password = bcrypt($user['password']);
            $p->tipusUsuari = $user['tipusUsuari'];
            $p->save();
        }
    }

    private $arrayUsers = array(
        array(
            'id' => '1',
            'usuari' => 'admin',
            'email' => 'moha633@gmail.com',
            'password' => 'reverse',
            'tipusUsuari' => '3'
        ),
        array(
            'id' => '2',
            'usuari' => 'moha',
            'email' => 'moha.elbarrad@gmail.com',
            'password' => 'reverse',
            'tipusUsuari' => '0'
        )
    );

    private function seedTreballadors(){
        DB::table('treballadors')->delete();

        foreach( $this->arrayTreballadors as $treballador) {
            $a = new Treballador;
            $a->nom = $treballador['nom'];
            $a->contrasenya = bcrypt($treballador['contrasenya']);
            $a->cognom1 = $treballador['cognom1'];
            $a->cognom2 = $treballador['cognom2'];
            $a->cifDni = $treballador['cifDni'];
            $a->email = $treballador['email'];
            $a->actiu = $treballador['actiu'];
            $a->save();
        }
    }

    private $arrayTreballadors = array(
        array(
            'nom' => 'admin',
            'contrasenya' => 'reverse',
            'cognom1' => 'admin',
            'cognom2' => 'admin',
            'cifDni' => '12345678A',
            'email' => 'admin@admin.com',
            'actiu' => 1
        ),
        array(
            'nom' => 'Moha',
            'contrasenya' => 'reverse',
            'cognom1' => 'El Barrad',
            'cognom2' => 'Sadequi',
            'cifDni' => '41595308S',
            'email' => 'moha.elbarrad@gmail.com',
            'actiu' => 1
        ),
        array(
            'nom' => 'usuari',
            'contrasenya' => 'reverse',
            'cognom1' => 'Prova',
            'cognom2' => '1',
            'cifDni' => '98765432Z',
            'email' => 'moha@moha.com',
            'actiu' => 1
        )
    );

    private function seedCentres(){
        DB::table('centres')->delete();

        foreach ($this->arrayCentres as $centre) {
            $c = new Centre;
            $c->client_id = $centre['client_id'];
            $c->nomCentre = $centre['nomCentre'];
            $c->descripcio = $centre['descripcio'];
            $c->poblacio = $centre['poblacio'];
            $c->domiciliCentre = $centre['domiciliCentre'];
            $c->save();
        }
    }

    private $arrayCentres = array(
        array(
            'client_id' => '1',
            'nomCentre' => 'CentreProva1',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
        array(
            'client_id' => '1',
            'nomCentre' => 'CentreProva2',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
        array(
            'client_id' => '1',
            'nomCentre' => 'ProvaCentre3',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
        array(
            'client_id' => '2',
            'nomCentre' => 'ProvaCentre4',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
        array(
            'client_id' => '2',
            'nomCentre' => 'ProvaCentre5',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
        array(
            'client_id' => '2',
            'nomCentre' => 'ProvaCentre6',
            'descripcio' => '......',
            'poblacio' => 'Olot',
            'domiciliCentre' => 'Av. Girona ...'
        ),
    );

    private function seedClients(){
        DB::table('clients')->delete();

        foreach ($this->arrayClients as $client) {
            $d = new Client;
            $d->nomClient = $client['nomClient'];
            $d->save();
        }
    }

    private $arrayClients = array(
        array(
            'nomClient' => 'Client1'
        ),
        array(
            'nomClient' => 'Client2'
        ),
        array(
            'nomClient' => 'Client3'
        ),
        array(
            'nomClient' => 'Client4'
        ),
        array(
            'nomClient' => 'Client5'
        ),
        array(
            'nomClient' => 'Client6'
        )
    );

    private function seedConceptes(){
        DB::table('conceptes')->delete();

        foreach ($this->arrayConceptes as $concepte) {
            $d = new Concepte;
            $d->nomTasca = $concepte['nomTasca'];
            $d->descripcio = $concepte['descripcio'];
            $d->actiu = $concepte['actiu'];
            $d->save();
        }
    }

    private $arrayConceptes = array(
        array(
            'nomTasca' => 'Neteja habitacions',
            'descripcio' => 'Neteja total de la habitació',
            'actiu' => 1
        ),
        array(
            'nomTasca' => 'Neteja hall',
            'descripcio' => 'Neteja de la entrada',
            'actiu' => 1
        ),
        array(
            'nomTasca' => 'Neteja Oficines',
            'descripcio' => 'Neteja de les oficines',
            'actiu' => 1
        ),
        array(
            'nomTasca' => 'Neteja passadissos',
            'descripcio' => 'Neteja passadissos',
            'actiu' => 0
        )
    );
}
