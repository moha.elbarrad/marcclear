<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentresConceptesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClientsCentresConceptes', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned()->default(0);
            $table->bigInteger('centre_id')->unsigned()->default(0);
            $table->bigInteger('concepte_id')->unsigned()->default(0);
            $table->integer('ordre');
            $table->time('tempsTeoric');
            $table->string('tipusUnitat');
            $table->boolean('actiu');


            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('centre_id')->references('id')->on('centres');
            $table->foreign('concepte_id')->references('id')->on('conceptes');
            $table->primary(array('client_id', 'centre_id', 'concepte_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centresConceptes');
    }
}
