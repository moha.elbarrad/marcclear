<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->bigInteger('id')->unsigned()->default(0);
            $table->string('usuari');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->boolean('tipusUsuari');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id')->references('id')->on('treballadors');
            $table->primary(array('id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
