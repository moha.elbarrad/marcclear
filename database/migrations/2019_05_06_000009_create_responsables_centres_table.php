<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsablesCentresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsablesCentres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('responsable_id')->unsigned();
            $table->bigInteger('centre_id')->unsigned();
            $table->timestamps();

            $table->foreign('responsable_id')->references('id')->on('users');
            $table->foreign('centre_id')->references('id')->on('centres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responsablesCentres');
    }
}
