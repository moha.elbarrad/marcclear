<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeinesTreballadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feinesTreballadors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('centre_id')->unsigned();
            $table->bigInteger('treballador_id')->unsigned();
            $table->bigInteger('concepte_id')->unsigned()->nullable();
            $table->date('data');
            $table->string('unitats')->nullable();
            $table->time('tempsTeoricUnitari')->nullable();
            $table->time('tempsTeoricTotal')->nullable();
            $table->time('tempsRealTotal')->nullable();
            $table->boolean('validat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feinesTreballadors');
    }
}
