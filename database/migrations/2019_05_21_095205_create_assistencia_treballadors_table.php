<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistenciaTreballadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistenciaTreballadors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('treballador_id')->unsigned()->default(0);
            $table->bigInteger('client_id')->unsigned()->default(0);
            $table->bigInteger('centre_id')->unsigned()->default(0);
            $table->date('dataRegistre');
            $table->time('horaRegistre');
            $table->boolean('entradasortida');
            $table->timestamps();

            $table->foreign('treballador_id')->references('id')->on('treballadors');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('centre_id')->references('id')->on('centres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistenciaTreballadors');
    }
}
