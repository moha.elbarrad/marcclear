<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//---------------------- USUARI API ---------------------------------------------
Route::get("/usuari/{id}", 'APIUser@show');
Route::get("/usuarisT", 'APIUser@index');
Route::get("/loginrespo/{id}", 'APIUser@comprovarUser');

Route::get("/treballadors", 'APITreballadors@index');
Route::get("/treballadorById/{id}", 'APITreballadors@llistarTreballadors');
Route::get("/treballadorByIdByPassword/{id}", 'APITreballadors@treballadorByPassword');
Route::get("/treballador/{nom}", 'APIResponsables@obtenirTreballadorbyNom');

Route::get("/responsable/{id}", 'APIResponsables@llistarResponsable');
Route::get("/comprovar/{idcomp}", 'APIResponsables@comprovar');

Route::get("/clients", 'APIResponsables@llistarClients');
Route::get("/client/{nom}", 'APIResponsables@obtenirClientbyNom');

Route::get("/centres", 'APIResponsables@llistarCentresT');
//Route::get("/centre/{nom}", 'APIResponsables@obtenirCentrebyNom');
Route::get("/centrebyid/{id}", 'APIResponsables@obtenirCentreById');
Route::get("/clientbyid/{id}", 'APIResponsables@obtenirClientById');

Route::get("/comprovarTasca/{nom}", 'APITasques@comprovarTasca');
Route::get("/tasques", 'APITasques@index');
Route::get("/conceptescentre/{id}", 'APITasques@llistarTasquesById');
Route::get("/tasquesEntrades/{id}", 'APIFeinaTreballador@tasquesEntrades');
//Route::get("/llistarTasques/{id}", 'APIFeinaTreballador@llistarTasquesByCentreClientData');
Route::get("/tascabyid/{id}", 'APITasques@obtenirNomTascaById');
Route::post("/addFeinaTreballador", 'APIFeinaTreballador@store');
Route::delete("/eliminarFeina/{id}", 'APIFeinaTreballador@destroy');

Route::post("/validar/{id}", 'APIFeinaTreballador@update');

Route::post("/afegirRespo", 'APIResponsables@store');
Route::post("/afegirTasca", 'APITasques@store');

Route::get("/centresbyidresponsable/{id}", 'APIResponsablesCentres@centresByIdResponsable');
Route::get("/obtenirData", 'APIFitxarTreballadors@ObtenirHora');
Route::get("/entradaosortida/{id}", 'APIFitxarTreballadors@EntradaOSortida');

//Route::get("/obtenircentreclient/{id}", 'APIResponsables@obtenirCentreClient');
Route::middleware('auth')->get("/obtenircentreclient/{id}", 'APIResponsables@obtenirCentreClient');

Route::post("/registre", 'APIFitxarTreballadors@store');
Route::post("/registrecamera", 'APIFitxarTreballadors@storeCamera');

//Route::post("/guardarcentreclient", 'APIResponsables@guardarCentreClient');
Route::middleware('auth')->post("/guardarcentreclient", 'APIResponsables@guardarCentreClient');

Route::get("/treballadorsF/{id}", 'APIResponsables@llistarTreballadorsFitxats');
Route::get("/obtenirhoresfitxar/{id}", 'APIResponsables@obtenirHoresFitxar');

Route::post("/actualitzarfila/{id}", 'APIFeinaTreballador@updateFila');

Route::get("/mostrarconceptestreballador/{id}", 'APIFeinaTreballador@mostrarConcepteTrebalaldor');

Route::get("/conceptebyid/{id}", 'APIFeinaTreballador@concepteById');
//----------------------- ADMIN API ---------------------------------------------
Route::get("clientsL", 'APIClients@index');
Route::get("centresbyidclient/{id}", 'APICentres@llistarCentresByIdClient');