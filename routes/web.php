<?php
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home_admin', 'HomeController@indexAdmin')->name('admin')->middleware('auth');
Route::get('/home_usuari', 'HomeController@indexUsuari')->name('usuari')->middleware('auth');
Route::get('/fitxarPersonal', 'HomeController@indexFitxar')->name('fitxar')->middleware('auth');
Route::get('/fitxarResponsable', 'HomeController@indexFitxarRespo')->name('fitxarR')->middleware('auth');
Route::get('/loginFitxar', 'HomeController@indexLoginRespo')->name('fitxarLR')->middleware('auth');
Route::get('/gestioUsuari', 'HomeController@gestioUsuari')->name("gestioUsuari")->middleware('auth');
Route::post('/gestioUsuari','HomeController@changePassword')->name('gestioUsuari')->middleware('auth');
Route::get("/contacte", 'ContacteController@index')->name("contacte")->middleware('auth');
Route::post("/contacte", 'ContacteController@enviarCorreu')->name("enviarMail")->middleware('auth');
Route::post("/iniciS", 'HomeController@comprovarUser')->name("loginok")->middleware('auth');
Route::post("/home_usuari", 'APIResponsables@guardarCentreClient')->name('guardarclientcentre')->middleware('auth');