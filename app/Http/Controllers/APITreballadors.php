<?php

namespace App\Http\Controllers;
use App\Treballador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class APITreballadors extends Controller
{
    public function llistarTreballadors($id){
        $treballador = Treballador::where('id', '=', $id)->get()->first();
        return response()->json($treballador);
    }

    public function index(){
        return response()->json(Treballador::all());
    }

    public function treballadorByPassword($id){
        $ids = explode("-", $id);
        $treballador = Treballador::where('id', '=', $ids[0])->get()->first();
        if ($treballador && Hash::check($ids[1], $treballador->contrasenya)) {
            return response()->json($treballador);
        }
    }
}
