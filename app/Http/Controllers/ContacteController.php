<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
class ContacteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contacte');
    }

    public function enviarCorreu(Request $request){
        $subject = $request->assumpte;
        $for = env('MAIL_USERNAME');
        $nom = $request->nom;
        Mail::send('correus/contacteM',$request->all(), function($msj) use($subject,$for,$nom){
            $msj->from("testcorreupractiques@gmail.com", $nom);
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back()->with("success", "Formulari enviat correctament");
    }
}
