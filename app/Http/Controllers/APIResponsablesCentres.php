<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ResponsableCentre;

class APIResponsablesCentres extends Controller
{
    public function centresByIdResponsable($id){

        $centres = ResponsableCentre::where('responsable_id', '=', $id)->get();
        return response()->json($centres);
    }
}
