<?php

namespace App\Http\Controllers;
use App\Client;
use App\Centre;
use App\Treballador;
use App\Variable;
use App\AssistenciaTreballador;
use App\FeinaTreballador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class APIResponsables extends Controller
{

    //funcions centres
    public function llistarCentresT(){
        return response()->json(Centre::all());
    }

    public function obtenirCentrebyNom($nomCentre){
        $centre = Centre::where('nomCentre', '=', $nomCentre)->get();
        return response()->json($centre);
    }

    public function obtenirCentreById($id){
        $centre = Centre::where('id', '=', $id)->get()->first();
        return response()->json($centre);
    }

    //funcions client
    public function llistarClients(){
        return response()->json(Client::all());
    }

    public function obtenirClientById($id){
        $client = Client::where('id', '=', $id)->get()->first();
        return response()->json($client);
    }

    public function obtenirClientbyNom($nomClient){
        $client = Client::where('nomClient', '=', $nomClient)->get();
        return response()->json($client);
    }

    //funcions treballador
    public function obtenirTreballadorbyNom($nomTreballador){
        $treballador = Treballador::where('nom', '=', $nomTreballador)->get();
        return response()->json($treballador);
    }

    /*public function guardarCentreClient(Request $request){

        $b = new Variable;
        $b->user_id =  $request->input('user_id');
        $b->client_id =  $request->input('client_id');
        $b->centre_id =  $request->input('centre_id');
        $b->save();

        return $b;
    }

    public function obtenirCentreClient($id){

        $clientCentre = Variable::where('user_id', '=', $id)->get()->first();

        return response()->json($clientCentre);
    }*/

    public function guardarCentreClient(Request $request){
        $request->put('centreID', $request->input('centre_id'));
        $request->put('clientID', $request->input('client_id'));

    }
    public function obtenirCentreClient(Request $request, $id){
        if($request->session()->has('centreID'));{
            $centreID = $request->session()->get('centreID');
        }
        
        if($request->session()->has('clientID')){
            $clientID = $request->session()->get('clientID');
        }

        if($clientID == 0 || $clientID == null || $centreID == 0 || $centreID == null){
            $ids[0] = 0;
            $ids[1] = 0;
        }else{
            $ids[0] = $centreID;
            $ids[1] = $clientID;
        }

        return $ids;
    }

    public function llistarTreballadorsFitxats($id){
        $ids = explode("-", $id);
        $dataD = $ids[2].'-'.$ids[3].'-'.$ids[4];
        $treballadors = FeinaTreballador::where('client_id', '=', $ids[0])->where('centre_id', '=', $ids[1])->where('data', '=', $dataD)->get();
        return response()->json($treballadors);
    }

    public function obtenirHoresFitxar($id){
        $ids = explode("-", $id);
        $dataR = $ids[3].'-'.$ids[4].'-'.$ids[5];
        $divisio = 0;
        $entrada = AssistenciaTreballador::select('horaRegistre')->where('client_id', '=', $ids[0])->where('centre_id', '=', $ids[1])->where('treballador_id', '=', $ids[2])->where('entradasortida', '=', '0')->where('dataRegistre', '=', $dataR)->get();
        $sortida = AssistenciaTreballador::select('horaRegistre')->where('client_id', '=', $ids[0])->where('centre_id', '=', $ids[1])->where('treballador_id', '=', $ids[2])->where('entradasortida', '=', '1')->where('dataRegistre', '=', $dataR)->get();
        for ($i=0; $i < sizeof($sortida); $i++) { 
            $tempsEntrada = explode(":", $entrada[$i]->horaRegistre);
            $tempsSortida = explode(":", $sortida[$i]->horaRegistre);
            $segonsEntrada = ((int)$tempsEntrada[0] * 3600) + ((int)$tempsEntrada[1] * 60) + ((int)$tempsEntrada[2]);
            $segonsSortida = ((int)$tempsSortida[0] * 3600) + ((int)$tempsSortida[1] * 60) + ((int)$tempsSortida[2]);
            $diferenciaSegons = $segonsSortida - $segonsEntrada;
            $divisio = $divisio + $diferenciaSegons;   
        }

        for ($j=0; $j < 3; $j++) { 
            $residu = $divisio;
            $divisio = intdiv($divisio, 60);
            $residu = $residu % 60;
            if($j == 0){
                if($residu < 10){
                    $hora = '0'.$residu;
                }else{
                    $hora = $residu;
                }
            }else{
                if($residu < 10){
                    $hora = '0'.$residu.':'.$hora;
                }else{
                    $hora = $residu.':'.$hora;
                }
            }
        }
        return $hora;
    }

    public static function eliminarVar($idUser){
        $b = Variable::where('user_id', '=', $idUser)->get()->first();
        if($b != null){
            $id = $b->id;
            $a = Variable::findOrFail($id);
            if($a != null){
                $a->delete();
            }
        }
    }
}
