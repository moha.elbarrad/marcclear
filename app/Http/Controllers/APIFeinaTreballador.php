<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FeinaTreballador;
use App\Concepte;

class APIFeinaTreballador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $b = new FeinaTreballador;
        $b->client_id =  $request->input('client_id');
        $b->centre_id =  $request->input('centre_id');
        $b->data =  $request->input('data');
        $b->concepte_id =  $request->input('concepte_id');
        $b->treballador_id =  $request->input('treballador_id');
        $b->unitats =  $request->input('unitats');
        $b->tempsRealTotal =  $request->input('tempsRealTotal');
        $b->validat =  $request->input('actiu');
        $b->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $a = FeinaTreballador::findOrFail($id);
        $a->validat = $request->input('actiu');
        $a->save();
    }

    public function updateFila(Request $request, $id){
        $a = FeinaTreballador::find($id);
        if($a == null){
            $b = new FeinaTreballador;
            $b->client_id =  $request->input('client_id');
            $b->centre_id =  $request->input('centre_id');
            $b->data =  $request->input('data');
            $b->concepte_id =  $request->input('concepte_id');
            $b->treballador_id =  $request->input('treballador_id');
            $b->unitats =  $request->input('unitats');
            $b->tempsRealTotal =  $request->input('tempsRealTotal');
            $b->validat =  $request->input('actiu');
            $b->save();
        }else{
            $a->concepte_id = $request->input('concepte_id');
            $a->tempsRealTotal = $request->input('tempsRealTotal');
            $a->unitats = $request->input('unitats');
            $a->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a = FeinaTreballador::findOrFail($id);
        $a->delete();
    }

    public function obtenirValidat($id){
        $ids = explode("-", $id);
        $dataD = $ids[0] + '-' + $ids[1] + '-' + $ids[2];
        $validat = FeinaTreballador::where('client_id', '=', $ids[3])->where('centre_id', '=', $ids[4])->where('data', '=', $dataD)->get();
        return response()->json($validat);
    }

    public function llistarTasquesByCentreClientData($id){
        $dades = explode("+",$id);
        $tasques = FeinaTreballador::where('client_id', '=', $dades[1])->where('client_id', '=', $dades[2])->where('data', '=', $dades[0])->get();
        return response()->json($tasques);
    }

    public function mostrarConcepteTrebalaldor($id){
        $ids = explode("-", $id);
        $dataR = $ids[3].'-'.$ids[4].'-'.$ids[5];
        $tasques = FeinaTreballador::where('client_id', '=', $ids[0])->where('centre_id', '=', $ids[1])->where('treballador_id', '=', $ids[2])->where('data', '=', $dataR)->orderBy('id')->get();
        return response()->json($tasques);
    }

    public function concepteById($id){
        $tasca = FeinaTreballador::where('id', '=', $id)->get()->first();
        return response()->json($tasca);
    }
}
