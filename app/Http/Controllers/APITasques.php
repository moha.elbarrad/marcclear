<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Concepte;
use App\ClientCentreConcepte;
use App\FeinaTreballador;

class APITasques extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Concepte::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $b = new Concepte();
        $b->nomTasca = $request->input('nomTasca');
        $b->descripcio = $request->input('descripcio');
        $b->actiu = $request->input('actiu');
        $b->save();

        return $b;
    }

    public function comprovarTasca($nomTasca){
        $nomTasca = str_replace('-', ' ', $nomTasca);
        $tasca = Concepte::where('nomTasca', '=', $nomTasca)->get();
        return response()->json($tasca);
    }

    public function llistarTasquesById($id){
        $ids = explode("-", $id);
        $tasques = ClientCentreConcepte::where('client_id', '=', $ids[0])->where('centre_id', '=', $ids[1])->orderBy('ordre')->get();
        return response()->json($tasques);
    }

    public function obtenirNomTascaById($id){
        $tasca = Concepte::where('id', '=', $id)->get();
        return response()->json($tasca);
    }
}
