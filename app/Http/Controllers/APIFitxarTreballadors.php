<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssistenciaTreballador;
use App\FeinaTreballador;
class APIFitxarTreballadors extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('entradasortida') == 1){
            $c = FeinaTreballador::where('treballador_id', '=', $request->input('treballador_id'))->get();
            $existeix = sizeof($c);
            if($existeix == 0){
                $a = new FeinaTreballador;
                $a->treballador_id = $request->input('treballador_id');
                $a->client_id = $request->input('client_id');
                $a->centre_id = $request->input('centre_id');
                $a->data = $request->input('dataRegistre');
                $a->tempsRealTotal = '00:00:00';
                $a->save();
            }
        }
        $b = new AssistenciaTreballador;
        $b->treballador_id =  $request->input('treballador_id');
        $b->dataRegistre =  $request->input('dataRegistre');
        $b->horaRegistre = $request->input('horaRegistre');
        $b->entradasortida =  $request->input('entradasortida');
        $b->client_id = $request->input('client_id');
        $b->centre_id = $request->input('centre_id');
        $b->save();

        return $b;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCamera(Request $request)
    {
        if($request->input('entradasortida') == 1){
            $c = FeinaTreballador::where('treballador_id', '=', $request->input('treballador_id'))->get();
            $existeix = sizeof($c);
            if($existeix == 0){
                $a = new FeinaTreballador;
                $a->treballador_id = $request->input('treballador_id');
                $a->client_id = $request->input('client_id');
                $a->centre_id = $request->input('centre_id');
                $a->data = $request->input('dataRegistre');
                $a->tempsRealTotal = '00:00:00';
                $a->save();
            }
        }
        $b = new AssistenciaTreballador;
        $b->treballador_id =  $request->input('treballador_id');
        $b->dataRegistre =  $request->input('dataRegistre');
        $b->horaRegistre = $request->input('horaRegistre');
        $b->entradasortida =  $request->input('entradasortida');
        $b->client_id = $request->input('client_id');
        $b->centre_id = $request->input('centre_id');
        $b->save();

        sleep(2);
        return $b;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function EntradaOSortida($id){
        $entrada = AssistenciaTreballador::where('treballador_id', '=', $id)->orderby('dataRegistre')->get();
        return response()->json($entrada);
    }

    public function ObtenirHora(){

        date_default_timezone_set('Europe/Madrid');

        $info = getdate();
        $date = $info['mday'];
        $month = $info['mon'];
        $year = $info['year'];
        $hour = $info['hours'];
        $min = $info['minutes'];
        $sec = $info['seconds'];

        $current_date[0] = "$hour:$min:$sec";
        $current_date[1] = "$date/$month/$year";
        $current_date[2] = "$year-$month-$date";

        return $current_date;
    }
}
