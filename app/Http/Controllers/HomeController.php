<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->tipusUsuari == 3){
            return view('home_admin');
        }else{
            return view('home_usuari');
        }
    }

    public function indexAdmin(){
        return view('home_admin');
    }

    public function indexUsuari(){
        return view('clientcentre');
    }


    public function indexFitxar(){
        return view('fitxarPersonal');
    }

    public function indexFitxarRespo(){
        return view('fitxarResponsable');
    }

    public function indexLoginRespo(){
        return view('loginFitxar');
    }

    public function gestioUsuari()
    {
        return view('gestioUsuari');
    }

    public function changePassword(Request $request)
    {
        $provaC = $request->get('new-password');
        if (! (Hash::check($request->get('current-password'), Auth::user()->password))) {

            return redirect()->back()->with("error", "La contrasenya actual no és correcte");
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "La new-password nova és la mateixa que l'actual, si us plau, canvia la nova contrasenya");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success", "Contrasenya canviada correctament !");
    }

    public function comprovarUser(Request $request){
        $usuari = $request->get('usuari');
        $contrasenya = $request->get('password');
    
        $respo = User::where('usuari', '=', $usuari)->get()->first();
        if(Hash::check($contrasenya, $respo->password)){
            return view('fitxarResponsable');
        }else{
            return view('loginFitxar');
        }
    }
}
