<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCentreConcepte extends Model
{
    protected $table = 'ClientsCentresConceptes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'centre_id', 'concepte_id', 'ordre', 'tempsTeoric', 'tipusQuantitat', 'actiu'
    ];
}
