<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treballador extends Model
{
    protected $table = 'treballadors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'contrasenya', 'cognom1', 'cognom2', 'cifDni', 'email', 'actiu'
    ];
}
