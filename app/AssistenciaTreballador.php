<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistenciaTreballador extends Model
{
    protected $table = 'assistenciaTreballadors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'treballador_id', 'dataRegistre', 'entradasortida'
    ];
}
