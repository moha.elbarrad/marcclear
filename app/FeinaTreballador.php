<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeinaTreballador extends Model
{
    protected $table = 'feinesTreballadors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'centre_id', 'treballador_id', 'concepte_id', 'data', 'unitats', 'tempsTeoricUnitari', 'tempsTeoricTotal', 'tempsRealTotal', 'validat'
    ];
}
