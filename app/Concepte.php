<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepte extends Model
{
    protected $table = 'conceptes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nomTasca', 'descripcio', 'actiu'
    ];
}
