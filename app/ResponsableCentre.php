<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsableCentre extends Model
{
    protected $table = 'responsablesCentres';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'responsable_id', 'centre_id'
    ];
}
